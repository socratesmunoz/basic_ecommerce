package com.basicecommerce.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Subcategoria {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idsubcategoria;
	
        @Column
	private int fk_to_categoria;
        
	@Column
	private String nombresubcategoria;
	
	@Column
	private String fotosubcategoria;

    /**
     * @return the idsubcategoria
     */
    public int getIdsubcategoria() {
        return idsubcategoria;
    }

    /**
     * @param idsubcategoria the idsubcategoria to set
     */
    public void setIdsubcategoria(int idsubcategoria) {
        this.idsubcategoria = idsubcategoria;
    }

    /**
     * @return the fk_to_categoria
     */
    public int getFk_to_categoria() {
        return fk_to_categoria;
    }

    /**
     * @param fk_to_categoria the fk_to_categoria to set
     */
    public void setFk_to_categoria(int fk_to_categoria) {
        this.fk_to_categoria = fk_to_categoria;
    }

    /**
     * @return the nombresubcategoria
     */
    public String getNombresubcategoria() {
        return nombresubcategoria;
    }

    /**
     * @param nombresubcategoria the nombresubcategoria to set
     */
    public void setNombresubcategoria(String nombresubcategoria) {
        this.nombresubcategoria = nombresubcategoria;
    }

    /**
     * @return the fotosubcategoria
     */
    public String getFotosubcategoria() {
        return fotosubcategoria;
    }

    /**
     * @param fotosubcategoria the fotosubcategoria to set
     */
    public void setFotosubcategoria(String fotosubcategoria) {
        this.fotosubcategoria = fotosubcategoria;
    }



}
