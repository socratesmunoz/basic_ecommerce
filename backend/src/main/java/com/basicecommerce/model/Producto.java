package com.basicecommerce.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Producto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idproducto;
	
	@Column
	private int fk_to_subcategoria;
        
        @Column
	private String nombreproducto;
        
        @Column
	private String descripcion;
	
        @Column
	private float peso;
        
        @Column
	private float preciousd;
        
	@Column
	private String foto1producto;
        @Column
	private String foto2producto;
        @Column
	private String foto3producto;

//Encapsulamiento        

    public int getIdproducto() {
        return idproducto;
    }

    public void setIdproducto(int idproducto) {
        this.idproducto = idproducto;
    }

    public int getFk_to_subcategoria() {
        return fk_to_subcategoria;
    }

    public void setFk_to_subcategoria(int fk_to_subcategoria) {
        this.fk_to_subcategoria = fk_to_subcategoria;
    }

    public String getNombreproducto() {
        return nombreproducto;
    }

    public void setNombreproducto(String nombreproducto) {
        this.nombreproducto = nombreproducto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public float getPreciousd() {
        return preciousd;
    }

    public void setPreciousd(float preciousd) {
        this.preciousd = preciousd;
    }

    public String getFoto1producto() {
        return foto1producto;
    }

    public void setFoto1producto(String foto1producto) {
        this.foto1producto = foto1producto;
    }

    public String getFoto2producto() {
        return foto2producto;
    }

    public void setFoto2producto(String foto2producto) {
        this.foto2producto = foto2producto;
    }

    public String getFoto3producto() {
        return foto3producto;
    }

    public void setFoto3producto(String foto3producto) {
        this.foto3producto = foto3producto;
    }
        

}
