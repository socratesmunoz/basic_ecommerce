package com.basicecommerce.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Categoria {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idcategoria;
	
	@Column
	private String nombrecategoria;
	
	@Column
	private String fotocategoria;

    /**
     * @return the idcategoria
     */
    public int getIdcategoria() {
        return idcategoria;
    }

    /**
     * @param idcategoria the idcategoria to set
     */
    public void setIdcategoria(int idcategoria) {
        this.idcategoria = idcategoria;
    }

    /**
     * @return the nombrecategoria
     */
    public String getNombrecategoria() {
        return nombrecategoria;
    }

    /**
     * @param nombrecategoria the nombrecategoria to set
     */
    public void setNombrecategoria(String nombrecategoria) {
        this.nombrecategoria = nombrecategoria;
    }

    /**
     * @return the fotocategoria
     */
    public String getFotocategoria() {
        return fotocategoria;
    }

    /**
     * @param fotocategoria the fotocategoria to set
     */
    public void setFotocategoria(String fotocategoria) {
        this.fotocategoria = fotocategoria;
    }


}
