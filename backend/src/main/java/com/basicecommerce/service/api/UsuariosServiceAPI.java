
package com.basicecommerce.service.api;

import com.basicecommerce.commons.GenericServiceAPI;
import com.basicecommerce.model.Usuarios;

/**
 * @author Sócrates Muñoz
 */

public interface UsuariosServiceAPI extends GenericServiceAPI<Usuarios, Integer>  { 
	
}