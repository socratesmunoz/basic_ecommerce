package com.basicecommerce.service.api;

import com.basicecommerce.commons.GenericServiceAPI;
import com.basicecommerce.model.Producto;

public interface ProductoServiceAPI extends GenericServiceAPI<Producto, Integer>  {
	
}
