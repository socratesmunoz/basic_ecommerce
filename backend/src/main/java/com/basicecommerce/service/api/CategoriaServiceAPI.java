package com.basicecommerce.service.api;

import com.basicecommerce.commons.GenericServiceAPI;
import com.basicecommerce.model.Categoria;

public interface CategoriaServiceAPI extends GenericServiceAPI<Categoria, Integer>  { //
	
}
