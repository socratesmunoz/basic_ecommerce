package com.basicecommerce.dao.api;

//import org.springframework.data.repository.CrudRepository;

import com.basicecommerce.model.Categoria;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoriaDaoAPI extends JpaRepository<Categoria, Integer> {

}
