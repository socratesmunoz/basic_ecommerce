package com.basicecommerce.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.basicecommerce.model.Subcategoria;
import com.basicecommerce.service.api.SubcategoriaServiceAPI;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@RequestMapping(value = "/apibasicecommerce/subcategorias")
@CrossOrigin({"*"})
public class SubcategoriaRestController {

	@Autowired
	private SubcategoriaServiceAPI subcategoriaServiceAPI;

	@GetMapping(value = "/all")
	public List<Subcategoria> getAll() {
		return subcategoriaServiceAPI.getAll();
	}
	
	@GetMapping(value = "/find/{id}")
	public Subcategoria find(@PathVariable int id) {
		return subcategoriaServiceAPI.get(id);
	}

	@PostMapping(value = "/save")
	public ResponseEntity<Subcategoria> save(@RequestBody Subcategoria subcategoria) {
		Subcategoria obj = subcategoriaServiceAPI.save(subcategoria);
		return new ResponseEntity<Subcategoria>(obj, HttpStatus.OK);
	}

	@GetMapping(value = "/delete/{id}")
	public ResponseEntity<Subcategoria> delete(@PathVariable int id) {
		
            Subcategoria subcategoria = subcategoriaServiceAPI.get(id);
		if (subcategoria != null) {
			subcategoriaServiceAPI.delete(id);
		}else {
			return new ResponseEntity<Subcategoria>(subcategoria, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<Subcategoria>(subcategoria, HttpStatus.OK);
	}

}
