package com.basicecommerce.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.basicecommerce.model.Producto;
import com.basicecommerce.service.api.ProductoServiceAPI;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@RequestMapping(value = "/apibasicecommerce/productos")
@CrossOrigin({"*"})
public class ProductoRestController {

	@Autowired
	private ProductoServiceAPI productoServiceAPI;

	@GetMapping(value = "/all")
	public List<Producto> getAll() {
		return productoServiceAPI.getAll();
	}
	
	@GetMapping(value = "/find/{id}")
	public Producto find(@PathVariable int id) {
		return productoServiceAPI.get(id);
	}

	@PostMapping(value = "/save")
	public ResponseEntity<Producto> save(@RequestBody Producto producto) {
		Producto obj = productoServiceAPI.save(producto);
		return new ResponseEntity<Producto>(obj, HttpStatus.OK);
	}

	@GetMapping(value = "/delete/{id}")
	public ResponseEntity<Producto> delete(@PathVariable int id) {
		
            Producto producto = productoServiceAPI.get(id);
		if (producto != null) {
			productoServiceAPI.delete(id);
		}else {
			return new ResponseEntity<Producto>(producto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<Producto>(producto, HttpStatus.OK);
	}

}
