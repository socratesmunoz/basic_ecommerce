package com.basicecommerce;

import com.basicecommerce.dao.api.ProductoDaoAPI;
import com.basicecommerce.dao.api.UsuariosDaoAPI;
import com.basicecommerce.model.Producto;
import com.basicecommerce.model.Usuarios;
import com.basicecommerce.service.api.UsuariosServiceAPI;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Sócrates Muñoz
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class Tests {

	@Autowired
	private UsuariosDaoAPI usuariosserviceapi;
        
       @Autowired
       private BCryptPasswordEncoder encoder;
        
        @Test
        public void crearusuarioTest(){
		Usuarios us = new Usuarios();
		us.setId(1);
		us.setNombre("admin");
		us.setClave(encoder.encode("admin123++"));		
		Usuarios retorno = usuariosserviceapi.save(us);
                    

            assertTrue(retorno.getClave().equalsIgnoreCase(us.getClave()));
        }
        
}
