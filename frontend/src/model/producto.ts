export class Producto{
    constructor(public idproducto:number = null, 
        public fk_to_subcategoria:number = null,
        public nombreproducto:string = null, 
        public descripcion:string = null, 
        public peso:number = null,
        public preciousd:number = null,
        public foto1producto:string = null,
        public foto2producto:string = null,
        public foto3producto:string = null){
}
}