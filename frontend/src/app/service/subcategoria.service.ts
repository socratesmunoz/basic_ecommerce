import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class SubcategoriaService {

baseUrl:string="http://localhost:8080/apibasicecommerce/subcategorias";

  constructor(private http:HttpClient ) { }
getAll(): Observable<any>{
  return this.http.get(this.baseUrl + "/all");
}
}